class MaFileDeNoms:
    def __init__(self):          #constructeur
        self.F=[]                #creation liste vide
        self.taille_max_file=10  #définit la taille max de la pile
        self.nbre_elements=0     #au départ, le nombre d'éléments présents dans la pile est égal à 0.

    def file_pleine(self):      # renvoie true si la pile est pleine, False si la pile est vide.
        if self.nbre_elements>=self.taille_max_file:
            return True
        else:
            return False

    def file_vide(self):        # renvoie true si la pile est vide, false si la pile est pleine
        if self.nbre_elements==0:
            return True
        else:
            return False

    def emfiler_element(self,element):
        if not self.file_pleine():
            self.F.append(element) #insère à la fin de la liste
            self.nbre_elements+=1

    def defiler_element(self):
        if not self.file_vide():
            self.nbre_elements-=1
            return self.F.pop(0)  # enleve le 1er element et le retourne

    def get_nbre_elements(self):
        return self.nbre_elements

    def get_F(self):
        return self.F

#test de la file
mystack = MaFileDeNoms()
mystack.emfiler_element(1)
mystack.emfiler_element(2)
mystack.emfiler_element(3)
print(mystack.get_F())
print(mystack.defiler_element())
print(mystack.defiler_element())
print(mystack.defiler_element())


