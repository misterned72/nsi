class MaPileDeNoms:
    def __init__(self):          #constructeur
        self.P=[]                #creation liste vide
        self.taille_max_pile=10  #définit la taille max de la pile
        self.nbre_elements=0     #au départ, le nombre d'éléments présents dans la pile est égal à 0.

    def pile_pleine(self):      # renvoie true si la pile est pleine, False si la pile est vide.
        if self.nbre_elements>=self.taille_max_pile:
            return True
        else:
            return False

    def pile_vide(self):         # renvoie true si la pile est vide, false si la pile est pleine
        if self.nbre_elements==0:
            return True
        else:
            return False

    def empiler_element(self,element):
        if not self.pile_pleine():
            self.P.insert(0,element)   #insère en tête de liste
            self.nbre_elements+=1

    def depiler_element(self):
        if not self.pile_vide():
            self.nbre_elements-=1
            return self.P.pop(0)  # enleve le sommet et le retourne

    def get_nbre_elements(self):
        return self.nbre_elements

    def get_P(self):
        return self.P

#test de la pile
mystack = MaPileDeNoms()
mystack.empiler_element("bête")
mystack.empiler_element("est")
mystack.empiler_element("toto")
print(mystack.get_P())
print(mystack.depiler_element())
print(mystack.depiler_element())
print(mystack.depiler_element())
print(mystack.depiler_element()) # juste un test pour bien montrer qu'il ne dépile plus puisque la pile est vide

